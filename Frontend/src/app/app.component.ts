import { Component } from '@angular/core';
import { Person } from './models/person';
import { PersonService } from './clients/person.service';
import { BirthdayResponse } from './models/birthday-response';
import { MatSnackBar } from '@angular/material/snack-bar';

/*
 * @author Sebastián Pinares Escobar
 * Component for manage user interactions
*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public person: Person = new Person();
  public response: BirthdayResponse;
  public citationRows: number = 2;
  private snackBarDuration = 3000;

  constructor(private personService: PersonService, private snackBar: MatSnackBar) { }

  public send(): void {
    //Send request
    this.personService.isBirthDay(this.person).subscribe(response => {
      this.response = response;
      this.citationRows = Math.ceil( response.poem.content.length / 330 ) + 1; //Dynamic rows for citation
    }, err => {
        this.snackBar.open('Error, '+err.error.message, 'Ok', {duration: this.snackBarDuration});
    });
  }
}
