import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../models/person';
import { Observable } from 'rxjs';
import { BirthdayResponse } from '../models/birthday-response';

/*
 * @author Sebastián Pinares Escobar
 * Service for calling API resources
*/

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private endpoint: string = 'http://localhost:8080/api/person/is-birthday';

  constructor( private http: HttpClient ) { }

  public isBirthDay(person: Person): Observable<BirthdayResponse>{
    return this.http.put<BirthdayResponse>(this.endpoint, person);
  }
}
