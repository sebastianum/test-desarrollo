import { Person } from './person';
import { Poem } from './poem';

export class BirthdayResponse {
  person: Person;
  age: number;
  remainingDays: number;
  poem: Poem;
}
