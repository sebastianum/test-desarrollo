package com.testdesarrollo.app.backend.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.testdesarrollo.app.backend.dto.Poem;

/*
 * @author Sebastián Pinares Escobar
 * Feign client to get poems 
*/

@FeignClient(name = "poems", url = "https://www.poemist.com/api/v1/randompoems")
public interface PoemClient {

	@RequestMapping(method = RequestMethod.GET)
	List<Poem> getPoems();
}
