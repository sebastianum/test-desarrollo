package com.testdesarrollo.app.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * @author Sebastián Pinares Escobar
 * Response for person request 
*/

@Getter
@Setter
@AllArgsConstructor
public class Response {
	private Person person;
	private int age;
	private int remainingDays;
	private Poem poem; //poems or remaining days for birthday
	
	
}
