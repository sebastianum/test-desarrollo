package com.testdesarrollo.app.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testdesarrollo.app.backend.dto.Person;
import com.testdesarrollo.app.backend.dto.Response;
import com.testdesarrollo.app.backend.services.PersonService;

import com.testdesarrollo.app.backend.dto.Error;

/*
 * @author Sebastián Pinares Escobar
 * Controller to manage actions over person data  
*/

@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping(path = "api/person")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@PutMapping(path = "/is-birthday")
	public ResponseEntity<?> isBirthday(@RequestBody Person person){
	
		try {
			return new ResponseEntity<Response>(personService.isBirthday(person), HttpStatus.OK);
		}
		catch(NullPointerException e) {	//Missing fields, just one mandatory (date)
			return new ResponseEntity<Error>(new Error(HttpStatus.INTERNAL_SERVER_ERROR, "debes completar los campos obligatorios"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(Exception e) { //Any other errors
			return new ResponseEntity<Error>(new Error(HttpStatus.INTERNAL_SERVER_ERROR, "intente nuevamente"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
