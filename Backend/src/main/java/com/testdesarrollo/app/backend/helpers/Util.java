package com.testdesarrollo.app.backend.helpers;

import java.util.List;
import java.util.Random;

/*
 * @author Sebastián Pinares Escobar
 * Common methods 
*/

public class Util {
	
	public static Object randomFromList(List<?> list) {
		int rand = new Random().nextInt(list.size());
		return list.get(rand);
	}
}
