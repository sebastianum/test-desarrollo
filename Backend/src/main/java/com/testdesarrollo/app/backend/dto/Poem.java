package com.testdesarrollo.app.backend.dto;

import lombok.Getter;
import lombok.Setter;

/*
 * @author Sebastián Pinares Escobar
 * Poem data 
*/

@Getter
@Setter
public class Poem {
	private String title;
	private String content;
	private String url;
	private Poet poet;
}
