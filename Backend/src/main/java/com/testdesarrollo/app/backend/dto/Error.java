package com.testdesarrollo.app.backend.dto;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * @author Sebastián Pinares Escobar
 * Readable error for frontend 
*/

@Getter
@Setter
@AllArgsConstructor
public class Error {
	private HttpStatus code;
	private String message;
}
