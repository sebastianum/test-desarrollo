package com.testdesarrollo.app.backend.services;

import com.testdesarrollo.app.backend.dto.Person;
import com.testdesarrollo.app.backend.dto.Response;

/*
 * @author Sebastián Pinares Escobar
 * Service for person actions  
*/

public interface PersonService {
	public Response	isBirthday(Person person);
}
