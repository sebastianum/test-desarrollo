package com.testdesarrollo.app.backend.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
 * @author Sebastián Pinares Escobar
 * Person data 
*/

@Getter
@Setter
@ToString
public class Person {
	private String name;
	private String lastname;
	private LocalDate birth;
}
