package com.testdesarrollo.app.backend.dto;

import lombok.Getter;
import lombok.Setter;

/*
 * @author Sebastián Pinares Escobar
 * Poet data 
*/


@Getter
@Setter
public class Poet {
	private String name;
	private String url;
}
