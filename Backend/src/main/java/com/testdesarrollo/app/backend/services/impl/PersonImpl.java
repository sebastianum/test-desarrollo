package com.testdesarrollo.app.backend.services.impl;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testdesarrollo.app.backend.clients.PoemClient;
import com.testdesarrollo.app.backend.dto.Person;
import com.testdesarrollo.app.backend.dto.Poem;
import com.testdesarrollo.app.backend.dto.Response;
import com.testdesarrollo.app.backend.helpers.Util;
import com.testdesarrollo.app.backend.services.PersonService;

/*
 * @author Sebastián Pinares Escobar
 * Service implementation for person actions  
*/

@Service
public class PersonImpl implements PersonService{

	@Autowired
	private PoemClient poemClient;
	
	@Override
	public Response isBirthday(Person person) throws NullPointerException{		
		Poem poem = null;
		
		LocalDate now = LocalDate.now();
		Period age = Period.between(person.getBirth(), now);
		
		//Assuming the birthday is over
		int remainingDays = (365 - now.getDayOfYear()) + person.getBirth().getDayOfYear();

		//If birthday is this year
		if(person.getBirth().getDayOfYear() > now.getDayOfYear())
			remainingDays = person.getBirth().getDayOfYear()  - now.getDayOfYear();
		
		//Send poem if today is birthday
		if(person.getBirth().getDayOfYear() == now.getDayOfYear())
			poem = (Poem) Util.randomFromList(poemClient.getPoems());
		
		return new Response(person, age.getYears(), remainingDays, poem);
	}
	
	
}
