# Test Desarrollo

Prueba práctica, proceso de selección "One Ops"

## Pre requisitos
* [npm 6.13.4 o superior](https://www.npmjs.com/get-npm) - Gestion de paquetes (Frontend)
* [Angular CLI](https://www.npmjs.com/package/@angular/cli) - Framework frontend
* [JDK 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) - Java Delevopment Kit
* [Apache Maven 3.6.3](https://maven.apache.org/download.cgi) - Gestión de dependencias (Backend)

## Uso
Primero, para levantar el backend del proyecto abrir una terminal en la carpeta "Backend" y ejecutar el siguiente comando

```bash
mvn spring-boot:run
```

En la carpeta "Frontend", abrir una nueva terminal y ejecutar los siguientes comandos en orden, para primero bajar las dependencias del proyecto y luego levantar el servidor
```bash
npm install
ng serve --open
```

Se abrirá automáticamente una pestaña en el navegador con la app
